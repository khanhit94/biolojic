<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileUploadRequest;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends Controller
{
    /**
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $awsFiles = Storage::disk('s3')->allFiles('files');
        $files = [];
        
        foreach ($awsFiles as $item) {
            $files[] = Storage::disk('s3')->url($item);
        }
        
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($files);
        $perPage = 1;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $items = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $items->setPath($request->url());
        
        // mở comment khi có UI
        //return view('files.index', compact('items'));
        return $items;
    }
    
    /**
     * @param FileUploadRequest $request
     * @return array
     */
    public function upload(FileUploadRequest $request)
    {
        $pathFile = [];
        if (($request->hasFile('pdf'))) {
            $files = $request->file('pdf');
            foreach ($files as $key => $file) {
                // If exist file, return error
                $filePath = 'files/' . $file->getClientOriginalName();
                if (Storage::disk('s3')->exists($filePath)) {
                    
                    // Show error on UI
                    continue;
                }
                
                $pathFile[$key] = $this->handleUpload($file);
            }
            return $pathFile;
        }
        
        return [];
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function delete(Request $request)
    {
//        $fileName = $request->fileName;
        
        // example
        $fileName = ['1.sample.pdf'];
        
        foreach ($fileName as $key => $file) {
            // If exist file, return error
            $filePath = 'files/' . $file;
            if (Storage::disk('s3')->exists($filePath)) {
                Storage::disk('s3')->delete($filePath);
            }
        }

        return true;
        
    }
}
