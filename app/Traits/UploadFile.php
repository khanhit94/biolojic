<?php

namespace App\Traits;
use Illuminate\Support\Facades\Storage;

trait UploadFile
{
    public function handleUpload($file)
    {
        $folderSave = 'files';

        $disk = Storage::disk('s3');
        $fileName  = $file->getClientOriginalName();
        $filePath = $folderSave . '/' . $fileName;
        $disk->putFileAs($folderSave, $file, $fileName);
        $disk->setVisibility($filePath, 'public');
        return $disk->url($filePath);
    }
}
